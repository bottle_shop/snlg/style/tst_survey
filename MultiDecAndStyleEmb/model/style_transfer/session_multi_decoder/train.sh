#!/bin/bash
#PBS -l nodes=1:ppn=20
#PBS -l walltime=168:00:00
#PBS -N session1_default
#PBS -A course
#PBS -q GpuQ


#source ~/v_python/bin/activate
#export THEANO_FLAGS=device=cuda*,floatX=float32,lib.cnmem=0.8
#THEANO_FLAGS=floatX=float32,device=cuda*,gpuarray.preallocate=1

export CUDA_ROOT=/usr/lib/cuda
export CUDA_HOME=/usr/lib/cuda
export PATH=${CUDA_HOME}/bin:$PATH
export LD_LIBRARY_PATH=${CUDA_HOME}/lib64:$LD_LIBRARY_PATH

#THEANO_FLAGS='floatX=float32,device=cuda*,gpuarray.preallocate=1,dnn.include_path=/usr/local/cuda/include/,dnn.library_path=/usr/local/cuda/lib64' python2 ./train_nmt.py 
THEANO_FLAGS='floatX=float32,device=cpu,gpuarray.preallocate=1' python2 ./train_nmt.py 



