# Text Style Transfer Paper List

# Parallel Data

- Evaluating prose style transfer with the Bible ('18) [[paper]](https://arxiv.org/ftp/arxiv/papers/1711/1711.04731.pdf)
- Shakespearizing Modern Language Using Copy-Enriched Sequence to Sequence Models (EMNLP'17 Workshop) [[paper]](https://arxiv.org/pdf/1707.01161.pdf)
- IMaT: Unsupervised Text Attribute Transfer via Iterative Matching and Translation (EMNLP'19) [[paper]](https://arxiv.org/pdf/1901.11333.pdf)
- QuaSE: Sequence Editing under Quantifiable Guidance (EMNLP'18) [[paper]](https://www.aclweb.org/anthology/D18-1420.pdf)
- Large-scale Hierarchical Alignment for Data-driven Text Rewriting (RANLP'19) [[paper]](https://arxiv.org/pdf/1810.08237.pdf)
- Semi-supervised Text Style Transfer: Cross Projection in Latent Space (EMNLP'19) [[paper]](https://arxiv.org/pdf/1909.11493.pdf)
- Harnessing Pre-Trained Neural Networks with Rules for Formality Style Transfer (EMNLP'19) [[paper]](https://www.aclweb.org/anthology/D19-1365.pdf)
- Formality Style Transfer with Hybrid Textual Annotations (arXiv'19) [[paper]](https://arxiv.org/pdf/1903.06353.pdf)
- Parallel Data Augmentation for Formality Style Transfer (ACL'20)[[paper]](https://arxiv.org/pdf/2005.07522.pdf)

# Non-Parallel Data
## Explicit Disentanglement(Explicit Style Keyword Replacement)
- Delete, Retrieve, Generate: a Simple Approach to Sentiment and Style Transfer (NAACL'18) [[paper]](https://arxiv.org/pdf/1804.06437.pdf)
- Transforming Delete, Retrieve, Generate Approach for Controlled Text Style Transfer (EMNLP'19) [[paper]](https://arxiv.org/pdf/1908.09368.pdf)
- A Hierarchical Reinforced Sequence Operation Method for Unsupervised Text Style Transfer (ACl'19) [[paper]](https://arxiv.org/pdf/1906.01833.pdf)
- Unpaired Sentiment-to-Sentiment Translation: A Cycled Reinforcement Learning Approach (ACL'18) [[paper]](https://arxiv.org/pdf/1805.05181.pdf)
- Learning Sentiment Memories for Sentiment Modification without Parallel Data (EMNLP'18) [[paper]](https://www.aclweb.org/anthology/D18-1138.pdf)

## Implicit Disentanglement
### Back-Translation
- Style Transfer Through Back-Translation (ACL'18) [[paper]](https://arxiv.org/pdf/1804.09000.pdf)
- Style Transfer as Unsupervised Machine Translation (arXiv'18) [[paper]](https://arxiv.org/pdf/1808.07894.pdf)

### Adversarial Learning
-  Adversarial text generation via feature-mover’s distance (NIPS'18) [[paper]](https://arxiv.org/pdf/1809.06297.pdf)
-  Style transfer in text: Exploration and evaluation (AAAI'18) [[paper]](https://arxiv.org/pdf/1711.06861.pdf)
-  Disentangled Representation Learning for Non-Parallel Text Style Transfer (ACL'19) [[paper]](https://www.aclweb.org/anthology/P19-1041.pdf)
-  Multiple Text Style Transfer by using Word-level Conditional Generative Adversarial Network with Two-Phase Training (EMNLP'19) [[paper]](https://www.aclweb.org/anthology/D19-1366.pdf)
-  Content preserving text generation with attribute controls (NIPS'18) [[paper]](https://arxiv.org/pdf/1811.01135.pdf)
- Paraphrase Diversification Using Counterfactual Debiasing (AAAI'19) [[paper]](https://www.aaai.org/ojs/index.php/AAAI/article/view/4665)
- Style transfer from non-parallel text by cross-alignment (NIPS'17) [[paper]](https://papers.nips.cc/paper/7259-style-transfer-from-non-parallel-text-by-cross-alignment.pdf)
- Unsupervised Text Style Transfer using Language Models as Discriminators (NIPS'18) [[paper]](https://arxiv.org/pdf/1805.11749.pdf)
- Utilizing non-parallel text for style transfer by making partial comparisons (IJCAI'19) [[paper]](https://www.ijcai.org/Proceedings/2019/0747.pdf)
- Adversarially regularized autoencoders (ICML'18) [[paper]](https://arxiv.org/pdf/1706.04223.pdf)
-  Language Style Transfer from Sentences with Arbitrary Unknown Styles (arXiv'18) [[paper]](https://arxiv.org/pdf/1808.04071.pdf)

### Attribute Control Generation
- Toward controlled generation of text (ICML'17) [[paper]](https://arxiv.org/pdf/1703.00955.pdf)
- Structured Content Preservation for Unsupervised Text Style Transfer (arXiv'18) [[paper]](https://arxiv.org/pdf/1810.06526.pdf)

## Without Disentanglement
### Attribute Control Generation
- Style Transformer: Unpaired Text Style Transfer without Disentangled Latent Representation (ACL'19) [[paper]](https://arxiv.org/pdf/1905.05621.pdf)
-  Unsupervised controllable text formalization (AAAI'19) [[paper]](https://arxiv.org/pdf/1809.04556.pdf)
-  Multiple-Attribute Text Rewriting (ICLR'19) [[paper]](https://openreview.net/pdf?id=H1g2NhC5KQ)
-  Shared-Private Encoder-Decoder for Text Style Adaptation (NAACL-HLT'18) [[paper]](https://www.aclweb.org/anthology/N18-1138.pdf)
-  Exploring Contextual Word-level Style Relevance for Unsupervised Style Transfer (ACL'20)[[paper]](https://arxiv.org/pdf/2005.02049.pdf)

### Entangled Latent Representation Edition
- Revision in Continuous Space: Fine-Grained Control of Text Style Transfer (AAAI'20) [[paper]](https://arxiv.org/pdf/1905.12304.pdf)
- Sequence to better sequence: continuous revision of combinatorial structures (ICML'17) [[paper]](https://people.csail.mit.edu/jonasmueller/info/Seq2betterSeq.pdf)
- Controllable Unsupervised Text Attribute Transfer via Editing Entangled Latent Representation (NIPS'19) [[paper]](https://arxiv.org/pdf/1905.12926.pdf)
- On Variational Learning of Controllable Representations for Text without Supervision [[paper]](https://arxiv.org/pdf/1905.11975.pdf)

### Reinforcement Learning 
- Reinforcement Learning Based Text Style Transfer without Parallel Training Corpus (NAACL'19) [[paper]](https://arxiv.org/pdf/1903.10671.pdf)
- A Dual Reinforcement Learning Framework for Unsupervised Text Style Transfer (IJCAI'19) [[paper]](https://arxiv.org/pdf/1905.10060.pdf)

### Probabilistic Modeling
- A Probabilistic Formulation of Unsupervised Text Style Transfer (ICLR'2020) [[paper]](https://openreview.net/pdf?id=HJlA0C4tPS)


# Interesting Text Style Application
- Style Matters! Investigating Linguistic Style in Online Communities (ICWSM'20)[[paper]](http://osamakhalid.org/docs/FULL-KhalidO.1032.pdf)
- Expertise Style Transfer: A New Task Towards Better Communication between Experts and Laymen (ACL'20)[[paper]](https://arxiv.org/pdf/2005.00701.pdf)
- Hooks in the Headline: Learning to Generate Headlines with Controlled Styles (ACL'20)[[paper]](https://arxiv.org/pdf/2004.01980.pdf)
- Story-level Text Style Transfer: A Proposal (ACL'20)[[paper]]()
