data_path = 'sentiment.test.1'
save_path = 'reference.1'

data=[]

with open(data_path, 'r') as f:
    for line in f:
        line = line.strip('\n')
        data.append(line+'\t'+line)

with open(save_path, 'w') as f:
    for line in data:
        f.write(line+'\n')
