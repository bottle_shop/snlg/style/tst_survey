#!/usr/bin/env bash

PROJECT_DIR_PATH="$PWD/$(dirname $0)/../"
cd ${PROJECT_DIR_PATH}

PYTHONPATH=${PROJECT_DIR_PATH} \
nohup python -u linguistic_style_transfer_model/main.py "$@" > log/style-transfer-train.log 2>&1 &
