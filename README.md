## Text_Style_Transfer 

This is the working repository for text style transfer survey. We are going to keep updating the results of test style transfer (TST)  models.

### Dataset 

**yelp**: is a corpus of restaurant reviews from Yelp collected in https://github.com/shentianxiao/language-style-transfer. The original Yelp reviews are on a 5 points rating scale. As part of data preprocessing, reviews with 3 points and above ratings are labeled as positive, while those below 3 points are labeled as negative. The reviews with an exact 3 points rating considered neutral and are excluded in this dataset.

*ps: we have preprocessed yelp dataset for all the models. We use the whole testset but some models only use a subset for testing.*

**GYAFC**: Since the GYAFC dataset is only free of charge for research purposes, we only publish the ouputs of TST models in the family and relationships domain, and the outputs are put in `outputs_all_models/`. If you want to download the train and validation dataset, please follow the guidance at https://github.com/raosudha89/GYAFC-corpus. And then, name the corpora of two styles as the yelp dataset.

### Results

The results on **Yelp** dataset of style transfer models included.

| Model      | ACC(%)    | self\-BLEU | CS         | WO         | PPL    | G\-Score  | H\-Score   |
| ---------- | --------- | ---------- | ---------- | ---------- | ------ | --------- | ---------- |
| DualRL     | 79\.0     | **58.3**   | 0\.97      | **0\.801** | 134    | **2\.29** | 0\.030     |
| DRLST      | 91\.2     | 7\.6       | 0\.904     | 0\.484     | **86** | 1\.41     | **0\.045** |
| DeleteOnly | 84\.2     | 28\.7      | 0\.893     | 0\.501     | 115    | 1\.80     | 0\.034     |
| Template   | 78\.2     | 48\.1      | 0\.850     | 0\.603     | 1959   | 1\.04     | 0\.002     |
| Ctrl\-Gen  | 89\.6     | 49\.5      | 0\.953     | 0\.707     | 384    | 1\.69     | 0\.010     |
| BST        | 83\.1     | 2\.3       | 0\.827     | 0\.076     | 261    | 0\.49     | 0\.015     |
| CAAE       | 82\.7     | 11\.2      | 0\.901     | 0\.277     | 145    | 1\.15     | 0\.027     |
| PTO        | 82\.3     | 57\.4      | **0\.982** | 0\.737     | 245    | 1\.94     | 0\.016     |
| ARAE       | 83\.2     | 18\.0      | 0\.874     | 0\.270     | 138    | 1\.31     | 0\.028     |
| B\-GST     | 89\.2     | 46\.5      | 0\.959     | 0\.649     | 216    | 1\.88     | 0\.018     |
| G\-GST     | 72\.7     | 52\.0      | 0\.967     | 0\.617     | 407    | 1\.55     | 0\.010     |
| DAST       | 90\.7     | 49\.7      | 0\.961     | 0\.705     | 323    | 1\.77     | 0\.012     |
| DAST\-C    | **93\.6** | 41\.2      | 0\.933     | 0\.560     | 450    | 1\.48     | 0\.009     |
| Multi\-Dec | 69\.6     | 17\.2      | 0\.887     | 0\.244     | 299    | 0\.99     | 0\.013     |
| Style\-Emb | 47\.5     | 31\.4      | 0\.926     | 0\.433     | 217    | 1\.31     | 0\.018     |
| UST        | 74\.0     | 41\.0      | 0\.929     | 0\.448     | 394    | 1\.36     | 0\.010     |
| SMAE       | 84\.4     | 14\.8      | 0\.907     | 0\.294     | 210    | 1\.315    | 0\.019     |
| PFST       | 85\.3     | 41\.7      | 0\.902     | 0\.527     | 104    | 2\.06     | 0\.038     |

The results on **GYAFC** dataset of style transfer models included.

| Model      | ACC(%)    | self\-BLEU | BLEU  | CS         | WO         | PPL    | G\-Score  | H\-Score   |
| ---------- | --------- | ---------- | ----- | ---------- | ---------- | ------ | --------- | ---------- |
| DualRL     | 56\.7     | **61\.6**  | 18\.8 | 0\.944     | 0\.447     | 122    | **1\.89** | 0\.032     |
| DRLST      | 71\.1     | 4\.2       | 2\.7  | 0\.909     | 0\.342     | 86     | 1\.04     | 0\.045     |
| DeleteOnly | 26\.0     | 35\.4      | 16\.2 | 0\.945     | 0\.431     | 82     | 1\.48     | **0\.047** |
| Template   | 51\.5     | 45\.1      | 19\.0 | 0\.943     | 0\.509     | 111    | 1\.81     | 0\.035     |
| Del&Retri  | 50\.6     | 22\.1      | 11\.8 | 0\.934     | 0\.345     | 94     | 1\.42     | 0\.041     |
| Ctrl\-Gen  | 73\.1     | 57\.0      | 15\.6 | 0\.943     | 0\.446     | 168    | 1\.82     | 0\.023     |
| BST        | 69\.7     | 0\.5       | 0\.5  | 0\.883     | 0\.04      | 69     | 0\.38     | 0\.042     |
| CAAE       | 72\.3     | 1\.8       | 1\.5  | 0\.896     | 0\.028     | 55     | 0\.51     | 0\.044     |
| ARAE       | 76\.2     | 4\.8       | 2\.2  | 0\.903     | 0\.042     | 77     | 0\.67     | 0\.040     |
| B\-GST     | 30\.3     | 22\.5      | 11\.6 | **0\.951** | **0\.557** | 117    | 1\.34     | 0\.034     |
| G\-GST     | 31\.0     | 20\.7      | 10\.2 | 0\.941     | 0\.556     | 127    | 1\.29     | 0\.031     |
| DAST       | 73\.1     | 50\.6      | 14\.3 | 0\.934     | 0\.350     | 204    | 1\.59     | 0\.019     |
| DAST\-C    | 78\.2     | 48\.5      | 13\.8 | 0\.927     | 0\.328     | 308    | 1\.42     | 0\.013     |
| Multi\-Dec | 22\.2     | 13\.4      | 5\.9  | 0\.911     | 0\.168     | 146    | 0\.76     | 0\.026     |
| Style\-Emb | 27\.7     | 8\.3       | 3\.6  | 0\.897     | 0\.102     | 136    | 0\.64     | 0\.027     |
| UST        | 23\.6     | 0\.5       | 0\.5  | 0\.881     | 0\.012     | **28** | 0\.27     | 0\.035     |
| SMAE       | 21\.6     | 6\.5       | 1\.2  | 0\.898     | 0\.079     | 74     | 0\.62     | 0\.046     |
| PFST       | 50\.8     | 55\.3      | 16\.5 | 0\.940     | 0\.466     | 200    | 0\.51     | 0\.020     |
| Human0     | 78\.1     | 20\.5      | 43\.5 | 0\.942     | 0\.393     | 80     | 1\.67     | 0\.048     |
| Human1     | **78\.7** | 18\.2      | 43\.2 | 0\.931     | 0\.342     | 199    | 1\.25     | 0\.020     |
| Human2     | 78\.2     | 18\.6      | 43\.4 | 0\.932     | 0\.354     | 192    | 1\.28     | 0\.021     |
| Human3     | 77\.4     | 18\.8      | 43\.5 | 0\.931     | 0\.354     | 196    | 1\.27     | 0\.020     |

The outputs of all models are put in `outputs_all_models/`, and we welcome other researchers pull request the outputs of your models. We will keep updating these two tables and `outputs_all_models/`, and correcting any error in our reproduce experiments.